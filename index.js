'use strict';

require('dotenv').config();

const SpotifyWebApi = require('spotify-web-api-node');
const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');
const fs = require('fs');
const morgan = require('morgan');

const {
  DEBUG=false,
  CLIENT_ID,
  CLIENT_SECRET,
  HOST,
  PORT,
  TOKEN_PATH,
  REFRESH_PATH,
  TIME_PATH,
  REFRESH_BEFORE_EXPIRE_MS,
  RETURN_TYPE,
  API_PATH='/current',
} = process.env;

if (typeof DEBUG === 'undefined' || DEBUG === false) {
  console.debug = function() {};
}

console.debug({
  CLIENT_ID,
  CLIENT_SECRET,
  HOST,
  PORT,
  TOKEN_PATH,
  REFRESH_PATH,
  TIME_PATH,
  REFRESH_BEFORE_EXPIRE_MS,
  RETURN_TYPE,
  DEBUG,
  API_PATH,
});

const RETURN_TYPES = {
  simple: 'simple',
  full: 'full'
};

const EMPTY_RESPONSE = {
  track: {},
  devices: {},
  device: {},
  name: '',
  artist: '',
  art: '',
  device: '',
};

if (!Object.values(RETURN_TYPES).includes(RETURN_TYPE.toLowerCase())) {
  throw new Error(
    'Return type must be one of [' + Object.values(RETURN_TYPES).toString() +
    '] got: ' + String(RETURN_TYPE)
  );
}

const app = express();
app.use(cors());
app.use(morgan('combined'));

// credentials are optional
const spotifyApi = new SpotifyWebApi({
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  redirectUri: `${HOST}:${PORT}`,
});


app.listen(PORT, () => {
  console.log('listening on port ' + PORT);
});

const authorizeURL = spotifyApi.createAuthorizeURL([
    'user-read-private', 'user-read-playback-state'
  ], 'start-state');

const storeToken = (tok, ref, exp) => {
  fs.writeFileSync(TOKEN_PATH, tok);
  fs.writeFileSync(REFRESH_PATH, ref);
  fs.writeFileSync(TIME_PATH, Buffer.from(String(Date.now() + exp*1000)));
}

const authSpotify = (code) => {
  spotifyApi.authorizationCodeGrant(code).then(
    function(data) {
      console.debug('The token expires in ' + data.body['expires_in']);
      console.debug('The access token is ' + data.body['access_token']);
      console.debug('The refresh token is ' + data.body['refresh_token']);

      // Set the access token on the API object to use it in later calls
      spotifyApi.setAccessToken(data.body['access_token']);
      spotifyApi.setRefreshToken(data.body['refresh_token']);
      storeToken(
        data.body['access_token'], data.body['refresh_token'],
        data.body['expires_in']
      );
    },
    function(err) {
      console.error('Something went wrong!', {err});
        const authorizeURL = spotifyApi.createAuthorizeURL([
          'user-read-private', 'user-read-playback-state'
        ], 'start-state');
      console.log('re-authorize', {authorizeURL});
    }
  );
};

app.get('/', (req, res) => {
  try {
    authSpotify(req.query.code);
    console.log('spotify is authenticated successfully');
  } catch (e) {
    console.error('authentication error!', e);
  } finally {
    res.sendStatus(200);
  }
});

const refreshAuth = () => {
  spotifyApi.refreshAccessToken().then(
    function(data) {
      console.debug('The access token has been refreshed!', data.body);

      // Save the access token so that it's used in future calls
      spotifyApi.setAccessToken(data.body['access_token']);
      storeToken(
        data.body['access_token'],
        spotifyApi.getRefreshToken(),
        data.body['expires_in']
      );
      const expireTime = +data.body['expires_in'] * 1000; // seconds
      setTimeout(refreshAuth, ((expireTime - REFRESH_BEFORE_EXPIRE_MS) || 0));
    },
    function(err) {
      console.error('Could not refresh access token', err);
      console.log('re-authorize', {authorizeURL});
    }
  );
};


const restoreToken = () => {
  try {
    const token = fs.readFileSync(TOKEN_PATH);
    spotifyApi.setAccessToken(token.toString());
    const refresh = fs.readFileSync(REFRESH_PATH);
    spotifyApi.setRefreshToken(refresh.toString());
    const expireTime = new Date(+fs.readFileSync(TIME_PATH).toString());
    console.debug(
      ((expireTime.getTime() - Date.now() - REFRESH_BEFORE_EXPIRE_MS) || 0)
      / 1000 / 60, 'mins until reauth'
    );
    // refresh at 1 minute before expiry
    setTimeout(refreshAuth, ((expireTime.getTime() - Date.now() - REFRESH_BEFORE_EXPIRE_MS) || 0));
  } catch (e) {
    console.error(
      'error restoring token', e,
      'you probably need to give a correct path to the token file'
    );
    console.log('please re-authorize @', {authorizeURL});
  }
};

try {
  restoreToken();
  refreshAuth();
} catch (e) {
  console.error(e, 'you might have to set your token file path correctly');
  console.log('please re-authorize @', {authorizeURL});
}

app.get(API_PATH, (req, res) => {
  spotifyApi.getMyDevices()
  .then(function(response_devices) {
    const availableDevices = response_devices.body.devices.filter(
      (e) => e.is_active
    );
    console.debug({availableDevices});
    if (availableDevices.length > 0) {
      spotifyApi.getMyCurrentPlayingTrack()
      .then(function(response_track) {
        try {
          console.debug({response_track: response_track.body});
          if (!response_track.item) {
            res.json(EMPTY_RESPONSE);
          } else if (
            RETURN_TYPE.toLowerCase() ===
            RETURN_TYPES.simple
          ) {
            res.json({
              name: response_track.body.item.name,
              artist: (
                response_track.body.item.artists[0] || {}
              ).name,
              art: (
                response_track.body.item.album.images[0] || {}
              ).url,
              device: (availableDevices[0] || {}).name
            });
          } else if (
            RETURN_TYPE.toLowerCase() ===
            RETURN_TYPES.full
          ) {
            res.json({
              track: response_track.body,
              devices: response_devices.body,
            });
          } else {
            res.status(400).json({
              message: 'return type not valid',
              data: {
                RETURN_TYPE,
              }
            });
          }
        } catch (e) {
          console.error(e);
          res.status(500).json({
            message: e.toString()
          });
        }
      }, function(err) {
        console.error('Something went wrong getting current track!', err);
        res.status(500).json({
          message: err.toString(),
        });
      });
    } else {
      res.json(EMPTY_RESPONSE);
    }
  }, function(err) {
    console.error('Something went wrong getting devices!', err);
    res.status(500).json({
      message: err.toString(),
    });
  });
})