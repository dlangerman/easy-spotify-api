# Easy spotify api

Two steps and you're off to the races. I personally use this on a dashboard in grafana but you can pretty much use it for whatever.

Prerequisite: Add a .env file at the project root (if you're running in a container, just use these as the environment in docker-compose, swarm, or kubernetes), heres a good default one to start with:

```
CLIENT_ID=<MY_CLIENT_ID>
CLIENT_SECRET=<MY_CLIENT_SECRET>
HOST=http://localhost
PORT=8888
TOKEN_PATH=./.token
REFRESH_PATH=./.refresh
TIME_PATH=./.last
REFRESH_BEFORE_EXPIRE_MS=60000
RETURN_TYPE=simple
API_PATH=/metrics
DEBUG=1
```

1. Create a spotify app [here](https://developer.spotify.com/dashboard/applications). 
    1.1 Take Note of the CLIENT_ID and CLIENT_SECRET
    1.2 Authorize the app by spinning up the server for the first time and visiting the authorization URL while the server is running, the `HOST` has to be a valid url that you can visit from the machine you click the link on, so localhost might not be what you want (if you're running on a remote machine, set HOST to be the IP/hostname of the remote machine, the PORT must be open to your machine).
    1.3 To eliminate dependencies on databases and such, the runtime data is persisted in files in a very naive, but functional way. This isn't meant to serve a bunch of people at once. 
2. Go ahead and run it
    2.1 just hit <HOST>:<PORT><API_PATH> with a get request and you'll see what you're currently listening to, or a blank resposne if nothing



Note because the access token and refresh token for your spotify account are stored in human readable files, this is NOT SECURE AT ALL, PLEASE DON'T EXPOSE THIS API ON A PUBLIC NETWORK: YOU HAVE BEEN WARNED, I am not responsible for any account or data theft. This is for hobbyists only.
