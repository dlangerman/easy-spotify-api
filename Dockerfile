FROM  node:lts-alpine

RUN  mkdir                      /src
COPY package.json               /src
WORKDIR                         /src

RUN                             ["cat", "package.json"]

RUN                             ["npm", "install","--production"]

COPY                            .                       /src

ENV                             NODE_ENV            production

CMD                             ["node", "index.js"]
